#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Glib::IO;
use Gtk4;

# Create a new application
my $app = Gtk4::Application->new('com.example.App', 'flags-none');

# When the application is launched…
$app->signal_connect(activate => sub {
  # … create a new window …
  my $win = Gtk4::ApplicationWindow->new($app);
  # … with a button in it …
  my $btn = Gtk4::Button->new_with_label('Hello World!');
  # … which closes the window when clicked
  $btn->signal_connect(clicked => sub { $win->close(); });
  $win->set_child($btn);
  $win->present();
});

# Run the application
$app->run();