#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;

BEGIN { require Gtk4; }

unless (eval { Gtk4->import; 1 }) {
  my $error = $@;
  if (eval { $error->isa('Glib::Error') &&
             $error->domain eq 'g-irepository-error-quark' })
  {
    BAIL_OUT("OS unsupported: $error");
  } else {
    BAIL_OUT("Cannot load Gtk4: $error");
  }
}

plan tests => 3;

is(Gtk4::check_version(3,0,0), 'GTK version too new (major mismatch)', 'check_version fail 1');
is(Gtk4::check_version(4,0,0), undef,                                  'check_version fail 2');
is(Gtk4::check_version(5,0,0), 'GTK version too old (major mismatch)', 'check_version fail 3');